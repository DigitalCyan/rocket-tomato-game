using Godot;
using System;

// Klasa koja predstavlja sve sto igrac moze unistavati za bodove i izbjegavati da izbjegne primanje stete

public class Hazard : Actor
{
    public float speed = 700; // Brzina instance

    private float life = 5; // Zdravlje instance

    // Postavljanje instance
    public override void _Ready()
    {
        health = 1;
        Random rng = new Random();
        Position = new Vector2(Position.x + rng.Next(-500,500) ,Position.y);
        LinearVelocity = new Vector2((-1 + 2 * (float)rng.NextDouble()) / 2, 1) * speed;
        AngularVelocity = (-1 + 2 * (float)rng.NextDouble()) * 30;
    }

    // Nakon 5 sekundi unistavamo instancu
    public override void _Process(float delta)
    {
        life -= delta;

        if(life <= 0){
            QueueFree();
        }
    }

    // Ovdije premoscujemo metodu OnDeath kako bi dodali posebnu logiku za ovu specificnu klasu
    public override void OnDeath()
    {
        GameManager.instance.addPoints(6);
        GameManager.instance.SpawnEntity("ent_explosion", GlobalTransform, true);
        QueueFree();
    }
}
