using Godot;
using System;

// Ova je klasa odgovorna za predstavljanje igraca (tj. rakete)

public class Player : Actor
{
    public Node2D visual; // Ova noda je roditelj svim vizualnim segmentima entiteta kao sto su spriteovi i particle systemi
    public Node2D cam; // Ova noda je roditelj kameri
    public Node2D muzzle; // Ova noda predstavlja poziciju gdje ce se igracevi projektili stvarati
    public Node2D posHazardSpawn; // Ova noda predstavlja osnovnu poziciju gdje ce se stvarati prepreke
    public float speed = 700; // Brzina zapreka
    public int lives = 2; // Zivoti

    private float defHazardSpawnTimer = 0.5f; // Svako koliko ce se sekunda stvoriti zapreka
    private float hazardTimer = 0; // Brojac koji odbrojava do sljedeceg stvaranja zapreke

    // Niz koji sadrzi nazive zapreka. Sobzirom da imamo zapreke spremljene u zasebne scene, mozemo ih instancirati putem metoda iz GameManager klase
    private string[] spawnList = new string[] { "ent_asteroid_1", "ent_asteroid_2", "ent_satelite", "ent_sputnik" };
    private Random rng = new Random(); // Generator "nasumicnih" brojeva

    // Postavljanje instance
    public override void _Ready()
    {
        visual = GetNode<Node2D>("./visual");
        cam = GetNode<Camera2D>("./camera");
        muzzle = GetNode<Node2D>("./visual/muzzle");
        posHazardSpawn = GetNode<Position2D>("./pos_asteroid_spawn");

        hazardTimer = defHazardSpawnTimer;

        ContactMonitor = true;
        ContactsReported = 30;

        Connect("body_entered", this, nameof(OnCollision));

        GetNode<Camera2D>("camera").Current = true;
    }

    // Petlja instance
    public override void _PhysicsProcess(float delta)
    {
        AngularVelocity = 0;

        Rotation = 0;

        CalculateMovement();

        CalculateRotation();

        SpawnHazard(delta);

        if (Input.IsActionJustPressed("fire"))
        {
            Fire();
        }
    }

    // Izracun kretanja ovisno o pritisnutim tipkama
    private void CalculateMovement()
    {
        Vector2 input = Vector2Utils.InputToVec2("down", "up", "right", "left");
        LinearVelocity = LinearVelocity.LinearInterpolate(input * speed, 0.1f);
    }

    // Izracun rotacije ovisno o pritisnutim tipkama
    private void CalculateRotation()
    {
        float targetRotation = Mathf.Deg2Rad(Vector2Utils.InputToInt("right", "left") * 15);
        visual.Rotation = visual.Rotation + (targetRotation - visual.Rotation) * 0.1f;
    }

    // Metoda odogovorna za pucanje
    private void Fire()
    {
        GameManager.instance.SpawnEntity("ent_projectile", muzzle.GlobalTransform);
    }

    // Metoda odogovorna za stvaranje zapreka
    private void SpawnHazard(float delta)
    {
        hazardTimer -= delta;

        if (hazardTimer <= 0)
        {
            hazardTimer = defHazardSpawnTimer;

            int index = rng.Next(0, spawnList.Length);
            GameManager.instance.SpawnEntity(spawnList[index], posHazardSpawn.GlobalTransform, false);
        }

    }

    // Metoda odgovorna za procesiranje detektiranog sudara
    private void OnCollision(Node other)
    {
        if (other.IsInGroup("hazard"))
        {
            other.QueueFree();
            Damage(45);
        }
    }

    // Premoscivanje metode za racunanje primljene stete. Sobzirom da igrac koristi zivote kao 
    // mjerilo zdravlja umjesto broja koji predstavlja kolicinu zdravlja potrebno je izmjeniti
    // logiku igraceva primanja stete tako da mu se pri izracunu stete oduzme 1 zivot nebitno
    // koliko stete primio

    public override void Damage(float damage)
    {
        lives -= 1;
        GameManager.instance.SpawnEntity("ent_explosion", GlobalTransform, true);
        if (lives == 1)
        {
            GetNode<Particles2D>("./visual/par_smoke").Emitting = true;
        }

        if (lives <= 0)
        {
            OnDeath();
        }
    }

    // Premoscivanje OnDeath metode da prilikom igraceva neuspjeha se ucita scn_gameover scena
    public override void OnDeath()
    {
        GameManager.instance.LoadScene("scn_gameover");
    }
}