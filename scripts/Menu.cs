using Godot;
using System;

// Ova klasa je odgovorna za zapocimanje igre

public class Menu : Node
{
    public void onStartGame(){
        GameManager.instance.setPoints(0);
        GameManager.instance.LoadScene("scn_intro");
    }

}
