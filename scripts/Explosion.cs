using Godot;

public class Explosion : Particles2D {

    private float life = 3; // Broj sekunda koliko ce instanca zivjeti.

    public override void _Ready()
    {
        OneShot = true; // OneShot field definira oce li se particle system okinuti samo jednom.
    }

    // U sljedecoj metodi oduzimamo proteklo vrijeme od zadanog vremena zivota te kad vrijeme zivota padne na ili ispod nule, brisemo instancu.
    public override void _PhysicsProcess(float delta)
    {
        life -= delta;

        if(life <= 0){
            QueueFree();
        }
    }
}