using Godot;

// Klasa odgovorna za procesiranje scene scn_space

public class Space : Node {

    private float stageTime = 30; // Koliko ce scena trajati
    private float time = 0; // Brojac vremena scene 
    private Node2D background; // Noda koja predstavlja pozadinu
    private Vector2 bgStartPos; // Pocetna pozicija pozadine
    
    // Inicijalizacija instance
    public override void _Ready()
    {
        background = GetNode<Node2D>("entities/ent_player/ent_bg");
        bgStartPos = background.Position;
    }

    // Petlja instance
    public override void _Process(float delta)
    {
        time += delta;

        if(time >= stageTime){
            GameManager.instance.LoadScene("scn_mars");
        }
        background.Position = bgStartPos.LinearInterpolate(Vector2.Zero , time / stageTime);
    }
    
}