using Godot;
using System;

// Klasa odgoovrna za ponasanje i funkcijonalnost letecih tanjura

public class UFO : Actor
{
    private float time; // Brojac vremena
    private float speed = 90; // Brzina spustanja
    private float strafeSpeed = 200; // Brzina kretanja lijevo/desno
    private float maxTilt = 15; // Maksimalan nagib u stupnjevima
    private float minY = 800; // Minimalna vrijednost koordinate y
                              // u slucaju da leteci tanjur ode nize 
                              // od minY igra se zavrsava i igrac je izgubio
    private int dir; // Smijer horizontalnog kretanja (-1 je lijevo a 1 je desno)

    // Inicijalizacija instance
    public override void _Ready()
    {
        dir = new Random().NextDouble() > 0.5f ? 1 : -1;
    }

    // Petlja instance
    public override void _Process(float delta)
    {
        time += delta;
        LinearVelocity = new Vector2(Mathf.Cos(time) * strafeSpeed * dir, speed);
        RotationDegrees = (float)Math.Cos(time) * maxTilt;
        if(GetGlobalTransformWithCanvas().origin.y >= minY){
            GameManager.instance.LoadScene("scn_gameover");
        }
    }

    // Premostenje OnDeath metode
    public override void OnDeath()
    {
        GameManager.instance.addPoints(6);
        GameManager.instance.SpawnEntity("ent_explosion", GlobalTransform, true);
        QueueFree();
    }
}
