using Godot;

// Ovo je osnovna klasa iz koje potjece sve sto ima zdravlje kao naprimjer igrac, prepreke (jer mogu biti unistene) i svemirci

public abstract class Actor : RigidBody2D
{
	// Export atribut nam omogucava mjenjanje vrijednosti unutar editora
	[Export]
	public float health = 100; // Svaki actor ce imati zdravlje

	// Uobicajeno smo rekli da se pri primanju stete actoru oduzme steta od zdravlja te se pozove metoda koja provjera je li actor mrtav.
	// Provjera da li je actor mrtav je odvojena od metode za procesiranje stete zbog modularnosti sustava. Takoder su obe postavljene kao virutalne
	// te se mogu overloadat u klasama koje nasljeduju iz ove.
	
	public virtual void Damage(float damage){ 
		health -= damage;
		CheckDeath();
	}

	public virtual void CheckDeath(){
		if(health <= 0){
			OnDeath();
		}
	}

	public virtual void OnDeath(){
		GetNode(".").QueueFree();
	}
}
