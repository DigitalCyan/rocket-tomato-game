using Godot;

public static class Vector2Utils {
    public static int InputToInt(string positive, string negative){
        int output = 0;
        if(Input.IsActionPressed(positive))
            output += 1;
        
        if(Input.IsActionPressed(negative))
            output -= 1;

        return output;
    }

    public static Vector2 InputToVec2(string positiveVertical, string negativeVertical, string positiveHorizontal, string negativeHorizontal){
        int vertical, horizontal = 0;

        vertical = Vector2Utils.InputToInt(positiveVertical, negativeVertical);
        horizontal = Vector2Utils.InputToInt(positiveHorizontal, negativeHorizontal);

        return new Vector2(horizontal, vertical);
    }
}