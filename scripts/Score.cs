using Godot;

// Klasa nasljeduje vec ugradenu Label klasu sto nam omoucuje njeno prosirenje te stvaranje nase label klase

public class Score : Label
{
    // Inicijalizacija instance
    public override void _Ready()
    {
        updateValue(GameManager.instance.Points);
        GameManager.instance.onPointsUpdate += updateValue;
    }

    // Metoda odogovrna za osvjezavanje labele
    private void updateValue(int value){
        Text = string.Format("Bodovi: {0}", value.ToString());
    }
}