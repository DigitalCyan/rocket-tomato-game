using Godot;
using System;

// Klasa odogovrna za funckiju poma

public class Tomato : Node2D
{
    private float timeUntilGrowth = 30; // Koliko sekunda je potrebno da narastu
    private float scale = 0; // Velicina pome

    // Petlja instance
    public override void _Process(float delta)
    {
        scale = Mathf.Clamp(scale + delta / timeUntilGrowth, 0, 1);
        Scale = new Vector2(scale, scale);
    }
}
