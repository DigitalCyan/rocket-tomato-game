using Godot;
using System;

// Klasa koja predstavlja igracev top u sceni Mars.

public class PlayerTurret : Node2D
{
    private Node2D head; // Noda koja predstavlja pomicni dio topa
    private Node2D muzzle; // Noda predstavlja vrh topa i mjesto odakle ce se stvarati projektili
    private Node2D cog; // Noda predstavlja zupcanik na topu
    private float time = 0; // Brojac vremena scene
    private float ang; // Kut pod kojim je top nagnut

    // Inicijalizacija instance
    public override void _Ready()
    {
        head = GetNode<Node2D>("head");
        cog = GetNode<Node2D>("cog");
        muzzle = head.GetNode<Node2D>("muzzle");
    }

    // Petlja instance
    public override void _Process(float delta)
    {      
        head.Rotation = Mathf.Lerp(head.Rotation, ang - Mathf.Pi / 2, this.GetProcessDeltaTime() * 5);
        cog.Rotation = -head.Rotation;

        if(Input.IsActionJustPressed("fire")){
            GameManager.instance.SpawnEntity("ent_cannon_projectile" ,muzzle.GlobalTransform, true);
        }
    }

    // Premoscivanje _Input metode
    // Metoda input pripada vec ugradenoj klasi Node te se pokrece svaki put kad se dogodi Input event
    public override void _Input(InputEvent @event)
    {
        if(@event is InputEventMouseMotion motion){
            Vector2 screenPos = head.GetGlobalTransformWithCanvas().origin;
            if(motion.Position.y < screenPos.y){
                ang = screenPos.AngleToPoint(motion.Position);
            }
        }
    }
}
