using Godot;
using System;

// Ova klasa je prva klasa koja se instancira pri pokretanju igre. Odgovorna je za inicijalizaciju prve scene.

public class Ignitor : Node
{
    [Export]
    public string mainScene = "scn_menu";

    public override void _Ready()
    {
        GameManager.instance.LoadScene(mainScene);
    }
}
