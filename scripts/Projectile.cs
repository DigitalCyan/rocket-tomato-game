using Godot;
using System;

// Ova klasa predstavlja igraceve projektile

public class Projectile : RigidBody2D
{
    public float speed = 1000; // Brzina projektila
    public float life = 5; // Vrijeme zivota projektila

    private float time = 0; // Brojac vremena
    private float damage = 5; // Steta projektila

    // Inicijalizacija instance
    public override void _Ready()
    {
        ContactMonitor = true;
        ContactsReported = 5;
        this.Connect("body_entered", this, nameof(OnHit));
        GameManager.instance.addPoints(-1);
    }

    // Petlja instance
    public override void _PhysicsProcess(float delta)
    {
        LinearVelocity = -Transform.y * speed ;
        time += delta;
        if(time >= life){
            QueueFree();
        }
    }

    // Metoda odogovrna za provodenje logike sudara
    public void OnHit(Node obj){
        if(obj.IsInGroup("hazard")){
            (obj as Actor).Damage(damage);
            obj.QueueFree();
        }
        QueueFree();
    }

}
