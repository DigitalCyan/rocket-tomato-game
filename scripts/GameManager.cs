using Godot;

// Ova je klasa odgovorna za izmjenu scena, pracenje bodova i instanciranje entiteta.

public class GameManager : Node2D
{
    public static GameManager instance; // Ovo polje koristimo unutar drugih klasa kad zelimo pirstupiti glavnoj instanci klase. (Istrazite: "singleton")

    private Node entities; // Ova noda sluzi kao roditelj za sve instancirane entitete, ovo nam olaksava pristup njima jer znamo gdje se nalaze.

    private Node root; // Ova noda predstavlja roditelja za sve instance scena, takoder olaksava laksu manipulaciju.

    private int points; // Ovdije spremamo bodove
    public int Points // Ovo svojstvo sluzi kao pristupnik polju points te dodaje logiku u manipulaciju vrijednosti. (Ne dozvoljava da bodovi budu negativni.);
    {
        get {return points;}
        set {
            if(value >= 0){
                points = value;
            }
        }
    }

    public delegate void UpdatePoints(int points); // Delegat definira signature metode za osvjezavanje broja bodova.

    public event UpdatePoints onPointsUpdate; // Event koji ce se izvrsavati pri promjeni bodova

    // Postavljanje GameManager-a
    public override void _Ready()
    {
        root = GetTree().Root.GetNode<Node>("game_root");
        instance = this;
    }

    // Metode za stvaranje entiteta
    public void SpawnEntity(string entityName, Transform2D transform, bool addOnTop = false)
    {
        PackedScene scene = ResourceLoader.Load<PackedScene>(string.Format("entities/{0}.tscn", entityName));
        Node2D entity = (Node2D)scene.Instance();
        entity.GlobalTransform = transform;
        entities.AddChild(entity);
        if (!addOnTop)
        {
            entities.MoveChild(entity, 0);
        }
    }

    public void SpawnEntity(string entityName, Vector2 position, float rotation, bool addOnTop = false)
    {
        PackedScene scene = ResourceLoader.Load<PackedScene>(string.Format("entities/{0}.tscn", entityName));
        Node2D entity = (Node2D)scene.Instance();
        entity.Position = position;
        entity.Rotation = rotation;
        entities.AddChild(entity);
        if (!addOnTop)
        {
            entities.MoveChild(entity, 0);
        }
    }

    public void SpawnEntity(string entityName, Vector2 position, bool addOnTop = false)
    {
        PackedScene scene = ResourceLoader.Load<PackedScene>(string.Format("entities/{0}.tscn", entityName));
        Node2D entity = (Node2D)scene.Instance();
        entity.Position = position;
        entities.AddChild(entity);
        if (!addOnTop)
        {
            entities.MoveChild(entity, 0);
        }
    }

    // Metode za izmjenu scena
    public void LoadScene(string sceneName)
    {
        onPointsUpdate = null;
        if (root.GetChildren().Count > 0)
        {
            root.GetChild(0).QueueFree();
        }

        PackedScene scene = ResourceLoader.Load<PackedScene>(string.Format("scenes/{0}.tscn", sceneName));
        Node sceneInstance = scene.Instance();

        root.AddChild(sceneInstance);
        entities = sceneInstance.GetNode("entities");
    }

    // Metoda za postavljanje bodova
    public void setPoints(int points)
    {
        FireOnPointsUpdate(Points);
        this.Points = points;
    }

    // Metoda za dodavanje bodova
    public void addPoints(int points)
    {
        this.Points += points;
        FireOnPointsUpdate(Points);
    }

    // Metoda za pokretanje onPointsUpdate eventa
    public void FireOnPointsUpdate(int points){
        if(onPointsUpdate != null){
            onPointsUpdate.Invoke(points);
        }
    }

}