using Godot;
using System;

// Ova klasa sadrzi metodu kojom se gumb moze sluziti kako bi se ucitala nova scene pri pritisku na njega

public class LoadMapButton : Node
{
    [Export]
    public string mapName = "scn_menu";
    public override void _Ready()
    {
        Connect("button_down", this, nameof(OnClick));
    }

    private void OnClick(){
        GameManager.instance.LoadScene(mapName);
    }
}
