using Godot;
using System;
using Godot.Collections;

// Ova je klasa odgovorna za brojne dogadaje na sceni scn_mars. Upravlja brojacem vremena i odgovorna je za stvaranje letecih tanjura.

public class Mars : Node
{
    [Export]
    private float stageTime = 30; // Koliko sekunda dugo ce trajati ova scena

    private float ufoTimer = 3; // Svako koliko sekunda ce se stvoriti leteci tanjur
    private float timer = 0; // Odgovorno za odbrojavanje vremena do stvaranja leteceg tanjura
    private float time = 0; // Odgovorno za brojanje vremena scene
    private Position2D posUFOSpawn; // Glavna pozicija na kojoj se stvara leteci tanjur

    private Random rng; // Generator "nasumicnih" brojeva
    private float offsetRange = 300; // Maksimalno odstupanje od x koordinate glavne pozicije stvaranja letecih tanjura
    
    // Postavljanje Instance
    public override void _Ready()
    {
        posUFOSpawn = GetNode<Position2D>("./entities/pos_ufo_spawn");
        GetNode<Camera2D>("entities/camera").Current = true;
        timer = ufoTimer;
        rng = new Random();
    }

    // Petlja instance
    public override void _Process(float delta)
    {
        time += delta;
        timer -= delta;
        if(timer <= 0){
            timer = ufoTimer;
            float offset = -offsetRange + ((float)rng.NextDouble() * 2 * offsetRange);
            GameManager.instance.SpawnEntity("ent_ufo", posUFOSpawn.GlobalPosition + new Vector2(offset, 0));
        }

        if(time > stageTime){
            GameManager.instance.LoadScene("scn_victory");
        }
    }

}
